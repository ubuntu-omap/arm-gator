/*
 * Example events provider
 *
 * Copyright (C) ARM Limited 2010-2011. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Similar entires must be present in events.xml file:
 *
 * <counter_set name="cpufreq_cntX">
 *   <counter name="cpufreq_cnt0"/>
 *   <counter name="cpufreq_cnt1"/>
 * </counter_set>
 * <category name="cpufreq" counter_set="cpufreq_cntX" per_cpu="no">
 *   <event event="0x0" title="Simulated" name="Sine" description="Sort-of-sine"/>
 *   <event event="0x1" title="Simulated" name="Triangle" description="Triangular wave"/>
 *   <event event="0x2" title="Simulated" name="PWM" description="PWM Signal"/>
 * </category>
 */

#include <linux/init.h>
#include <linux/io.h>
#include <linux/ratelimit.h>
//#include <linux/clk.h>
#include <linux/cpufreq.h>

#include "gator.h"

#define CPUFREQ_COUNTERS_NUM 1

static struct {
	unsigned long enabled;
	unsigned long event;
	unsigned long key;
} cpufreq_counter;

static int cpufreq_buffer[CPUFREQ_COUNTERS_NUM * 2];

/* Adds cpufreq_cntX directories and enabled, event, and key files to /dev/gator/events */
static int gator_events_cpufreq_create_files(struct super_block *sb,
		struct dentry *root)
{
	struct dentry *dir;

	dir = gatorfs_mkdir(sb, root, "cpufreq_cnt");
	if (WARN_ON(!dir))
		return -1;
	gatorfs_create_ulong(sb, dir, "enabled",
			&cpufreq_counter.enabled);
	gatorfs_create_ulong(sb, dir, "event",
			&cpufreq_counter.event);
	gatorfs_create_ro_ulong(sb, dir, "key",
			&cpufreq_counter.key);

	return 0;
}

static int gator_events_cpufreq_start(void)
{
	return 0;
}

static void gator_events_cpufreq_stop(void)
{
}

static int cpufreq_simulate(void)
{
	int freq = (int)(cpufreq_quick_get(smp_processor_id())/1000);
	//printk("cpufreq=%d MHz\n", freq);
	return freq;
}

static int gator_events_cpufreq_read(int **buffer)
{
	int len = 0;

	/* System wide counter - read from one core only */
	if (smp_processor_id())
		return 0;

	if (cpufreq_counter.enabled) {
		cpufreq_buffer[len++] = cpufreq_counter.key;
		cpufreq_buffer[len++] = cpufreq_simulate();
		printk("cpufreq=%d\n", (int)(cpufreq_buffer[len-1]));
	}
	
	if (buffer)
		*buffer = cpufreq_buffer;

	return len;
}

static struct gator_interface gator_events_cpufreq_interface = {
	.create_files = gator_events_cpufreq_create_files,
	.start = gator_events_cpufreq_start,
	.stop = gator_events_cpufreq_stop,
	.read = gator_events_cpufreq_read,
};

/* Must not be static! */
int __init gator_events_cpufreq_init(void)
{
	cpufreq_counter.enabled = 0;
	cpufreq_counter.key = gator_events_get_key();

	return gator_events_install(&gator_events_cpufreq_interface);
}
gator_events_init(gator_events_cpufreq_init);
